package com.maetimo.rssreader.data.service.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class PostDto(
        @JacksonXmlProperty(localName = "link")
        var link: String,

        @JacksonXmlProperty(localName = "title")
        var title: String,

        @JacksonXmlProperty(localName = "pubDate")
        var date: Date,

        @JacksonXmlProperty(localName = "description")
        var description: String
)