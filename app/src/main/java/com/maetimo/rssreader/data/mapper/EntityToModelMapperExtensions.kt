package com.maetimo.rssreader.data.mapper

import com.maetimo.rssreader.data.db.entity.ChannelEntity
import com.maetimo.rssreader.data.db.entity.ChannelWithPosts
import com.maetimo.rssreader.data.db.entity.PostEntity
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.data.repository.model.PostModel

fun ChannelEntity.toModel() = ChannelModel(title, address, icon, lastUpdate, emptyList())
fun ChannelWithPosts.toModel() = ChannelModel(channel.title, channel.address, channel.icon, channel.lastUpdate, posts.map { it.toModel() })
fun PostEntity.toModel() = PostModel(title, description, link, date)