package com.maetimo.rssreader.data.service.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ChannelDto(
        @JacksonXmlProperty(localName = "title")
        var title: String,

        @JacksonXmlElementWrapper(useWrapping = false)
        @JacksonXmlProperty(localName = "item")
        var posts: List<PostDto>,

        @JacksonXmlProperty(localName = "image")
        var image: ImageDto?,

        var address: String?
)