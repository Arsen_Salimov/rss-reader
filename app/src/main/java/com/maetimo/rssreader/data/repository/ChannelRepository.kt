package com.maetimo.rssreader.data.repository

import com.maetimo.rssreader.App
import com.maetimo.rssreader.data.mapper.toEntity
import com.maetimo.rssreader.data.mapper.toModel
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.data.service.RssService
import com.maetimo.rssreader.utils.rx.RetryWithDelay
import io.reactivex.Flowable

import io.reactivex.Single

object ChannelRepository {
    private val channelDao = App.database.channelDao()

    fun addChanel(address: String): Single<ChannelModel> {
        return RssService
                .getChannelByAddress(address)
                .map { channel ->
                    channelDao.insertWithPosts(channel)
                    return@map channel
                }
    }

    fun getChannel(address: String): Flowable<ChannelModel> {
        val remoteChannel = getRemoteChannel(address)
                .map { channel ->
                    channelDao.updateWithPosts(channel)
                    return@map channel
                }


        return getCachedChannel(address).concatWith(remoteChannel)
    }

    private fun getCachedChannel(address: String): Single<ChannelModel> {
        return channelDao.getChannelByAddress(address)
                .map { it.toModel() }
    }

    private fun getRemoteChannel(address: String): Single<ChannelModel> {
        return RssService.getChannelByAddress(address)
                .retryWhen(RetryWithDelay(4, 1000))
    }


    fun getAllChannels(): Flowable<List<ChannelModel>> {
        return channelDao.getAllChannels()
                .map { channels -> channels.map { it.toModel() } }
    }
}
