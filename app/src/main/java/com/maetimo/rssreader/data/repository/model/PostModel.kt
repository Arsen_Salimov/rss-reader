package com.maetimo.rssreader.data.repository.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class PostModel(
        val title: String,
        val description: String,
        val link: String,
        val date: Date
) : Parcelable
