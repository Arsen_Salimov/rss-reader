package com.maetimo.rssreader.data.db

import com.maetimo.rssreader.data.db.entity.ChannelEntity
import com.maetimo.rssreader.data.db.entity.PostEntity

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.maetimo.rssreader.data.db.converter.DateConverter

@Database(entities = [ChannelEntity::class, PostEntity::class], version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun channelDao(): ChannelDao
}
