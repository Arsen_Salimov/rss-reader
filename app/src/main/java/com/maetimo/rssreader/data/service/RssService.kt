package com.maetimo.rssreader.data.service

import com.ctc.wstx.stax.WstxInputFactory
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.dataformat.xml.XmlFactory
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.maetimo.rssreader.data.mapper.toModel
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.data.service.dto.Rss

import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.Request

object RssService {
    private val client: OkHttpClient = OkHttpClient().newBuilder()
            .followRedirects(true)
            .followRedirects(true)
            .build()

    fun getChannelByAddress(address: String): Single<ChannelModel> {
        return Single.create { emitter ->
            if (!emitter.isDisposed) {

                try {
                    val request = Request.Builder()
                            .url(address)
                            .build()
                    val response = client.newCall(request).execute()

                    val ifactory = WstxInputFactory()
                    val xf = XmlFactory(ifactory)
                    val mapper = XmlMapper(xf)
                    val module = SimpleModule()
                    mapper.registerModule(module)
                    mapper.registerModule(KotlinModule())

                    val url = response.request().url().toString()
                    val channel = mapper.readValue(response.body()?.charStream(), Rss::class.java).channel
                    channel.address = url

                    emitter.onSuccess(channel.toModel())
                } catch (e: Exception) {
                    emitter.onError(e)
                }
            }
        }

    }
}
