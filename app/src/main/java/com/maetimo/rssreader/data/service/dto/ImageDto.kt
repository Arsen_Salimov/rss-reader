package com.maetimo.rssreader.data.service.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class ImageDto(
        @JacksonXmlProperty(localName = "link")
        val link: String,

        @JacksonXmlProperty(localName = "url")
        val url: String
)