package com.maetimo.rssreader.data.db

import androidx.room.*
import com.maetimo.rssreader.data.db.entity.ChannelEntity
import com.maetimo.rssreader.data.db.entity.ChannelWithPosts
import com.maetimo.rssreader.data.db.entity.PostEntity
import com.maetimo.rssreader.data.mapper.toEntity
import com.maetimo.rssreader.data.repository.model.ChannelModel

import io.reactivex.Flowable
import io.reactivex.Single
import java.util.*

@Dao
interface ChannelDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(channels: List<ChannelEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(channel: ChannelEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllPost(post: List<PostEntity>)

    @Update
    fun update(channel: ChannelEntity): Int

    @Transaction
    fun insertWithPosts(channelModel: ChannelModel) {
        val channelWithPostEntity = channelModel.toEntity()
        channelWithPostEntity.channel.lastUpdate = Date()
        insert(channelWithPostEntity.channel)

        insertAllPost(channelWithPostEntity.posts.map {
            it.channelId = channelModel.address
            it
        })
    }

    @Transaction
    fun updateWithPosts(channelModel: ChannelModel) {
        val channelWithPostEntity = channelModel.toEntity()
        channelWithPostEntity.channel.lastUpdate = Date()

        update(channelWithPostEntity.channel)

        insertAllPost(channelWithPostEntity.posts.map {
            it.channelId = channelModel.address
            it
        })
    }

    @Query("SELECT * FROM channels ORDER BY title")
    fun getAllChannels(): Flowable<List<ChannelEntity>>

    @Transaction
    @Query("SELECT * FROM channels WHERE address = :address")
    fun getChannelByAddress(address: String): Single<ChannelWithPosts>
}
