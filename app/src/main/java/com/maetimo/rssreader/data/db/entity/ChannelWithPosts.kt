package com.maetimo.rssreader.data.db.entity

import androidx.room.Embedded
import androidx.room.Relation

data class ChannelWithPosts(
        @Embedded
        val channel: ChannelEntity,

        @Relation(parentColumn = "address", entityColumn = "channel_id")
        var posts: List<PostEntity>
)