package com.maetimo.rssreader.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "channels")
data class ChannelEntity(
        @PrimaryKey
        var address: String,
        var title: String? = null,
        var icon: String? = null,

        @ColumnInfo(name = "last_update")
        var lastUpdate: Date? = null
)