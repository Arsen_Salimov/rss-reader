package com.maetimo.rssreader.data.db

import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.maetimo.rssreader.App
import com.maetimo.rssreader.data.db.entity.ChannelEntity
import java.util.concurrent.Executors

class DbCreateCallback : RoomDatabase.Callback() {
    override fun onCreate(db: SupportSQLiteDatabase) {
        super.onCreate(db)

        Executors.newSingleThreadExecutor().execute {
            App.database.channelDao().insertAll(listOf(
                    ChannelEntity(address = "https://habr.com/ru/rss/all/all/", title = "Все публикации подряд", icon = "https://habr.com/images/logo.png"),
                    ChannelEntity(address = "https://lenta.ru/rss/news", title = "Lenta.ru : Новости", icon = "https://assets.lenta.ru/small_logo.png"),
                    ChannelEntity(address = "http://www.vesti.ru/vesti.rss", title = "Вести.Ru")

            ))
        }
    }
}