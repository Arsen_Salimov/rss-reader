package com.maetimo.rssreader.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "posts")
data class PostEntity(
        @PrimaryKey
        var link: String,

        var title: String,

        var description: String,

        var date: Date,

        @ColumnInfo(name = "channel_id")
        var channelId: String?
)