package com.maetimo.rssreader.data.mapper

import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.data.repository.model.PostModel
import com.maetimo.rssreader.data.service.dto.ChannelDto
import com.maetimo.rssreader.data.service.dto.PostDto
import java.util.*

fun PostDto.toModel() = PostModel(title, description, link, date)
fun ChannelDto.toModel() = ChannelModel(title, address!!, image?.url, Date(), posts.map { it.toModel() })