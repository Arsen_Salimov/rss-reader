package com.maetimo.rssreader.data.mapper

import com.maetimo.rssreader.data.db.entity.ChannelEntity
import com.maetimo.rssreader.data.db.entity.ChannelWithPosts
import com.maetimo.rssreader.data.db.entity.PostEntity
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.data.repository.model.PostModel

fun PostModel.toEntity() = PostEntity(link, title, description, date, null)
fun ChannelModel.toEntity() = ChannelWithPosts(ChannelEntity(address, title, icon, lastUpdate), posts.map { it.toEntity() })