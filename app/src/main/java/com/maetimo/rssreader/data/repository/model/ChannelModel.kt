package com.maetimo.rssreader.data.repository.model

import java.util.*

data class ChannelModel(
        val title: String?,
        val address: String,
        val icon: String?,
        val lastUpdate: Date?,
        val posts: List<PostModel> = emptyList()
)