package com.maetimo.rssreader.data.service.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Rss(
        @JacksonXmlProperty(localName = "channel")
        var channel: ChannelDto
)
