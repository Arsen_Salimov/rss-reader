package com.maetimo.rssreader.utils.date

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    @JvmStatic
    fun toSimpleString(date: Date): String {
        val format = SimpleDateFormat("dd/MM/yyy HH:mm", Locale("ru"))
        return format.format(date)
    }
}

fun Date.toSimpleString() = DateUtils.toSimpleString(this)