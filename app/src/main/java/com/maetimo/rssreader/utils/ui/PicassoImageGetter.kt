package com.maetimo.rssreader.utils.ui

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.Html
import android.widget.TextView
import com.maetimo.rssreader.R
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.lang.Exception

class PicassoImageGetter(val target: TextView) : Html.ImageGetter {
    override fun getDrawable(s: String): Drawable? {
        val drawable = BitmapDrawablePlaceHolder()
        Picasso
                .get()
                .load(s)
                .placeholder(R.drawable.ic_image_black)
                .into(drawable)

        return drawable
    }

    inner class BitmapDrawablePlaceHolder: BitmapDrawable(), Target {
        var drawable: Drawable? = null

        override fun draw(canvas: Canvas) {
            drawable?.draw(canvas)
        }

        private fun setDrawableAndRefreshTarget(drawable: Drawable) {
            this.drawable = drawable

            val sourceWidth = drawable.intrinsicWidth
            val sourceHeight = drawable.intrinsicHeight

            val targetWidth = target.width
            val targetHeight = (targetWidth * sourceHeight) / sourceWidth

            drawable.setBounds(0, 0, targetWidth, targetHeight)
            setBounds(0, 0, targetWidth, targetHeight)

            target.text = target.text
        }

        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

        }

        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {

        }

        @Suppress( "DEPRECATION")
        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            setDrawableAndRefreshTarget(BitmapDrawable(bitmap))
        }
    }
}
