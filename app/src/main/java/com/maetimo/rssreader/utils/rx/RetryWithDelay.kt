package com.maetimo.rssreader.utils.rx

import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

class RetryWithDelay(private val maxRetries: Int, private val retryDelayMillis: Long) : ((Flowable<Throwable>) -> Flowable<Long>) {
    private var retryCount: Int = 0

    init {
        this.retryCount = 0
    }

    override fun invoke(attempts: Flowable<Throwable>): Flowable<Long> {
        return attempts
                .flatMap { throwable ->
                    if (++retryCount < maxRetries) {
                        Flowable.timer(retryDelayMillis, TimeUnit.MILLISECONDS)
                    } else {
                        Flowable.error(throwable)
                    }
                }
    }
}