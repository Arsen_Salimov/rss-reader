package com.maetimo.rssreader

import android.app.Application

import com.maetimo.rssreader.data.db.AppDatabase

import androidx.room.Room
import com.maetimo.rssreader.data.db.DbCreateCallback


class App : Application() {

    override fun onCreate() {
        super.onCreate()

        instance = this
        database = Room
                .databaseBuilder(this, AppDatabase::class.java, "db")
                .addCallback(DbCreateCallback())
                .build()
    }

    companion object {
        lateinit var instance: App
        lateinit var database: AppDatabase
    }
}