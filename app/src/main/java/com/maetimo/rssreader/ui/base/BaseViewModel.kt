package com.maetimo.rssreader.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel()