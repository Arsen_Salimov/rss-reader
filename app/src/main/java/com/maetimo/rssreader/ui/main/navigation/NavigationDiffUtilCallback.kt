package com.maetimo.rssreader.ui.main.navigation

import androidx.recyclerview.widget.DiffUtil
import com.maetimo.rssreader.data.repository.model.ChannelModel

class NavigationDiffUtilCallback(
        private val oldChannels: List<ChannelModel>,
        private val newChannels: List<ChannelModel>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldChannels[oldItemPosition] == newChannels[newItemPosition]
    }

    override fun getOldListSize(): Int {
        return oldChannels.size
    }

    override fun getNewListSize(): Int {
        return newChannels.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldChannels[oldItemPosition] == newChannels[newItemPosition]
    }
}