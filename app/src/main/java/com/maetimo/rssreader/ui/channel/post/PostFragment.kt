package com.maetimo.rssreader.ui.channel.post

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View

import com.maetimo.rssreader.R
import com.maetimo.rssreader.databinding.FragmentPostBinding
import com.maetimo.rssreader.ui.base.BaseFragment
import com.maetimo.rssreader.ui.main.MainActivity
import com.maetimo.rssreader.utils.ui.PicassoImageGetter

class PostFragment : BaseFragment<MainActivity, PostViewModel, FragmentPostBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_post

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.body.movementMethod = LinkMovementMethod()

        if (arguments != null) {
            val args = PostFragmentArgs.fromBundle(arguments!!)
            val post = args.post

            viewModel.setPost(post, PicassoImageGetter(binding.body))
        }
    }
}
