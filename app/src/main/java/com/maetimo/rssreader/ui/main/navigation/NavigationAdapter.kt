package com.maetimo.rssreader.ui.main.navigation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListUpdateCallback
import androidx.recyclerview.widget.RecyclerView
import com.maetimo.rssreader.R
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.databinding.ItemNavigationBinding
import com.maetimo.rssreader.utils.date.toSimpleString
import com.squareup.picasso.Picasso

class NavigationAdapter : RecyclerView.Adapter<NavigationAdapter.ViewHolder>() {
    private var lastSelectedPosition: Int? = null
    var onSelectionListener: (ChannelModel) -> Unit = {}
    private val internalChannels: MutableList<ChannelModel> = arrayListOf()

    fun setChannels(channels: List<ChannelModel>) {
        val diffResult = DiffUtil.calculateDiff(NavigationDiffUtilCallback(internalChannels, channels))

        this.internalChannels.clear()
        this.internalChannels.addAll(channels)

        diffResult.dispatchUpdatesTo(this)
        diffResult.dispatchUpdatesTo(object: ListUpdateCallback{
            override fun onChanged(position: Int, count: Int, payload: Any?) {
            }

            override fun onMoved(fromPosition: Int, toPosition: Int) {
                if (lastSelectedPosition == fromPosition) {
                    lastSelectedPosition = toPosition
                }
            }

            override fun onInserted(position: Int, count: Int) {
            }

            override fun onRemoved(position: Int, count: Int) {
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemNavigationBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return internalChannels.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val channel = internalChannels[position]
        holder.bind(channel, position)
    }

    inner class ViewHolder(private val binding: ItemNavigationBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(channel: ChannelModel, position: Int) {
            binding.isSelected = position == lastSelectedPosition

            binding.root.setOnClickListener {
                if (position != lastSelectedPosition) {
                    onSelectionListener(channel)
                }

                if (lastSelectedPosition != null) {
                    notifyItemChanged(lastSelectedPosition!!)
                }

                lastSelectedPosition = position
                binding.isSelected = true
            }

            val lastUpdateMessage = binding.lastUpdate.context.getString(R.string.last_update)
            val never = binding.lastUpdate.context.getString(R.string.never)

            binding.lastUpdate.text = String.format(lastUpdateMessage, channel.lastUpdate?.toSimpleString() ?: never)
            binding.channelName.text = channel.title

            if (channel.icon != null) {
                Picasso.get().load(channel.icon).into(binding.channelIcon)
            }
        }
    }
}