package com.maetimo.rssreader.ui.channel.list

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View

import com.maetimo.rssreader.ui.main.MainActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.maetimo.rssreader.R
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.databinding.FragmentChannelBinding
import com.maetimo.rssreader.ui.base.BaseFragment
import io.reactivex.disposables.CompositeDisposable

class ChannelFragment : BaseFragment<MainActivity, ChannelViewModel, FragmentChannelBinding>() {
    private var adapter: ChannelAdapter = ChannelAdapter()
    private var channel: ChannelModel? = null

    override fun getLayoutId(): Int = R.layout.fragment_channel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter.onClickListener = { post -> parentActivity.navigateToPostDetails(channel!!, post) }
        binding.postsRecyclerView.adapter = adapter
        binding.postsRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.postsRecyclerView.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        binding.container.setOnRefreshListener {
            if (arguments != null) {
                val args = ChannelFragmentArgs.fromBundle(arguments!!)

                loadChannel(args.channelAddress)
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (arguments != null) {
            val args = ChannelFragmentArgs.fromBundle(arguments!!)

            loadChannel(args.channelAddress, true)
        }
    }

    private fun loadChannel(address: String, firstTime: Boolean = false) {
        disposable.add(
                viewModel.loadChannel(address, firstTime)
                        .subscribe({
                            channel = it
                            adapter.setPosts(it.posts)
                        }, this::handleError)
        )
    }

    private fun handleError(error: Throwable) {
        Log.e(tag, "error occurred during loading channel", error)
        Snackbar.make(binding.container, R.string.add_channel_error_message, Snackbar.LENGTH_LONG).show()
    }
}
