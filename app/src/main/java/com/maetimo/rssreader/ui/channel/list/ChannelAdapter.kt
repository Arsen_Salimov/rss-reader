package com.maetimo.rssreader.ui.channel.list

import android.view.LayoutInflater
import android.view.ViewGroup

import com.maetimo.rssreader.data.repository.model.PostModel
import com.maetimo.rssreader.databinding.ItemPostBinding
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

class ChannelAdapter : RecyclerView.Adapter<ChannelAdapter.ViewHolder>() {
    var onClickListener: (PostModel) -> Unit = {}
    private var internalPosts: MutableList<PostModel> = arrayListOf()

    fun setPosts(posts: List<PostModel>) {
        val diffResult = DiffUtil.calculateDiff(ChannelDiffUtilCallback(internalPosts, posts))

        this.internalPosts.clear()
        this.internalPosts.addAll(posts)

        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemPostBinding.inflate(layoutInflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val postDto = internalPosts[position]
        holder.bind(postDto)
    }

    override fun getItemCount(): Int {
        return internalPosts.size
    }

    inner class ViewHolder(private val binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.root), LifecycleOwner {
        private val lifecycleRegistry = LifecycleRegistry(this)

        internal fun bind(post: PostModel) {
            this.binding.root.setOnClickListener { onClickListener(post) }
            this.binding.post = post;
        }

        override fun getLifecycle(): Lifecycle {
            return lifecycleRegistry
        }
    }
}
