package com.maetimo.rssreader.ui.main.navigation

import androidx.lifecycle.MutableLiveData
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.ui.base.BaseViewModel

class NavigationItemViewModel : BaseViewModel() {
    val channel = MutableLiveData<ChannelModel>()
}