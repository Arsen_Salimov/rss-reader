package com.maetimo.rssreader.ui.channel.add

import androidx.lifecycle.MutableLiveData
import com.maetimo.rssreader.data.repository.ChannelRepository
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.ui.base.BaseViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AddChannelViewModel : BaseViewModel() {
    val requestInProgress = MutableLiveData<Boolean>()

    fun addChannel(channelAddress: String): Single<ChannelModel> {
        return ChannelRepository
                .addChanel(channelAddress)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { requestInProgress.value = true }
                .doFinally { requestInProgress.value = false }
    }
}