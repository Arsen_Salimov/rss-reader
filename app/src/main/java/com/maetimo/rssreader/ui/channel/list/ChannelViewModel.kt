package com.maetimo.rssreader.ui.channel.list

import com.maetimo.rssreader.data.repository.ChannelRepository
import com.maetimo.rssreader.data.repository.model.PostModel
import androidx.lifecycle.MutableLiveData
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.ui.base.BaseViewModel
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ChannelViewModel : BaseViewModel() {
    val posts = MutableLiveData<List<PostModel>>()
    val isLoading = MutableLiveData<Boolean>()
    val isRefreshing = MutableLiveData<Boolean>()

    fun loadChannel(channelLink: String, initial: Boolean = false): Flowable<ChannelModel> {
        return ChannelRepository.getChannel(channelLink)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isLoading.value = initial }
                .doFinally {
                    isLoading.value = false
                    isRefreshing.value = false
                }
                .doOnNext { channel ->
                    posts.value = channel.posts
                }
    }
}
