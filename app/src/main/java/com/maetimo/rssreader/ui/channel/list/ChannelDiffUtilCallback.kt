package com.maetimo.rssreader.ui.channel.list

import androidx.recyclerview.widget.DiffUtil
import com.maetimo.rssreader.data.repository.model.PostModel

class ChannelDiffUtilCallback(
        private val oldChannels: List<PostModel>,
        private val newChannels: List<PostModel>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldChannels[oldItemPosition] == newChannels[newItemPosition]
    }

    override fun getOldListSize(): Int {
        return oldChannels.size
    }

    override fun getNewListSize(): Int {
        return newChannels.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldChannels[oldItemPosition] == newChannels[newItemPosition]
    }
}