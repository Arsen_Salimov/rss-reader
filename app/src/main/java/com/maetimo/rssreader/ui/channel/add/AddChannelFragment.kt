package com.maetimo.rssreader.ui.channel.add

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View

import com.maetimo.rssreader.databinding.FragmentAddChannelBinding
import android.view.inputmethod.InputMethodManager
import com.google.android.material.snackbar.Snackbar
import com.maetimo.rssreader.R
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.ui.base.BaseFragment
import com.maetimo.rssreader.ui.main.MainActivity

class AddChannelFragment : BaseFragment<MainActivity, AddChannelViewModel, FragmentAddChannelBinding>() {
    private var errorSnackbar: Snackbar? = null

    override fun getLayoutId(): Int {
        return R.layout.fragment_add_channel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.addNewChannelBtn.setOnClickListener { onAddNewChannelClick() }
        binding.editText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                showKeyboard()
            } else {
                closeKeyboard()
            }
        }
    }

    private fun onAddNewChannelClick() {
        val address = binding.editText.text.toString()
        disposable.add(viewModel.addChannel(address)
                .subscribe(this::onSuccess, this::onError)
        )
    }

    override fun onDetach() {
        errorSnackbar?.dismiss()

        super.onDetach()
    }

    @Suppress("unused")
    private fun onSuccess(channel: ChannelModel) {
        val mainActivity = activity as MainActivity?
        mainActivity?.navigateFromAddNewChannel()
    }

    private fun onError(error: Throwable) {
        Log.e(tag, "Error occurred during adding new channel", error)
        closeKeyboard()

        if (errorSnackbar == null || !errorSnackbar!!.isShown) {
            errorSnackbar = Snackbar.make(binding.container, R.string.add_channel_error_message, Snackbar.LENGTH_INDEFINITE)
            errorSnackbar?.show()
        }
    }

    private fun closeKeyboard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    private fun showKeyboard() {
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }
}
