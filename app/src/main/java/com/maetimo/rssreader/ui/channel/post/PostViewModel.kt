package com.maetimo.rssreader.ui.channel.post

import android.os.Build
import android.text.Html
import android.text.Spanned
import androidx.lifecycle.MutableLiveData
import com.maetimo.rssreader.data.repository.model.PostModel
import com.maetimo.rssreader.ui.base.BaseViewModel

class PostViewModel : BaseViewModel() {
    val postBody = MutableLiveData<Spanned>()
    val postTitle = MutableLiveData<String>()

    fun setPost(post: PostModel, imageGetter: Html.ImageGetter) {
        postBody.value = fromHtml(post.description, imageGetter)
        postTitle.value = post.title
    }

    @Suppress("DEPRECATION")
    private fun fromHtml(html: String, imageGetter: Html.ImageGetter): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY, imageGetter, null)
        } else {
            Html.fromHtml(html, imageGetter, null)
        }
    }
}
