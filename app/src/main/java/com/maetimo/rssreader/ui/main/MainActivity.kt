package com.maetimo.rssreader.ui.main

import android.os.Bundle
import android.view.MenuItem

import com.maetimo.rssreader.R

import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.maetimo.rssreader.data.repository.ChannelRepository
import com.maetimo.rssreader.data.repository.model.ChannelModel
import com.maetimo.rssreader.data.repository.model.PostModel
import com.maetimo.rssreader.databinding.ActivityMainBinding
import com.maetimo.rssreader.ui.base.BaseActivity
import com.maetimo.rssreader.ui.main.navigation.NavigationAdapter
import com.maetimo.rssreader.ui.channel.BlankFragmentDirections
import com.maetimo.rssreader.ui.channel.list.ChannelFragmentDirections
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val disposable = CompositeDisposable()
    private var adapter = NavigationAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupNavigation()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    private fun setupNavigation() {
        setSupportActionBar(binding.toolbar)

        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.noChannels, R.id.ChannelFragment), binding.drawerLayout)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)

        binding.navigationList.layoutManager = LinearLayoutManager(this)
        binding.navigationList.adapter = adapter
        binding.addNewChannelBtn.setOnClickListener {
            binding.drawerLayout.closeDrawers()
            navController.navigate(R.id.AddChannelFragment)
        }
        adapter.onSelectionListener = { channelModel ->
            binding.drawerLayout.closeDrawer(GravityCompat.START)
            navigateToChannel(channelModel)
        }

        loadChannels()
    }

    private fun loadChannels() {
        disposable.add(ChannelRepository
                .getAllChannels()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { channels ->
                    adapter.setChannels(channels)
                }
        )
    }

    fun navigateFromAddNewChannel() {
        navController.popBackStack()
    }

    private fun navigateToChannel(channel: ChannelModel) {
        val appName = getString(R.string.app_name)
        val title = channel.title ?: appName

        if (navController.currentDestination?.id == R.id.ChannelFragment) {
            navController.navigate(ChannelFragmentDirections.actionPostListFragmentSelf(title, channel.address))
        } else {
            navController.navigate(BlankFragmentDirections.actionStartToPostListFragment(title, channel.address))
        }
    }

    fun navigateToPostDetails(channel: ChannelModel, post: PostModel) {
        val appName = getString(R.string.app_name)
        val title = channel.title ?: appName
        navController.navigate(ChannelFragmentDirections.actionPostListFragmentToPostDetailsFragment(post, title))
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = item.onNavDestinationSelected(navController)
            || super.onOptionsItemSelected(item)


    override fun onSupportNavigateUp() = navController.navigateUp(appBarConfiguration)
}
