package com.maetimo.rssreader.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.maetimo.rssreader.BR
import io.reactivex.disposables.CompositeDisposable
import java.lang.reflect.ParameterizedType

abstract class BaseFragment<A : BaseActivity, VM : BaseViewModel, B : ViewDataBinding> : Fragment() {
    protected val viewModel: VM by lazy {
        ViewModelProviders.of(this).get(getVMClass())
    }
    protected lateinit var binding: B
    protected val disposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        binding.setVariable(BR.viewModel, viewModel)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }

    @LayoutRes
    abstract fun getLayoutId(): Int

    @Suppress("UNCHECKED_CAST")
    private fun getVMClass(): Class<VM> {
        return (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<VM>
    }


    @Suppress("UNCHECKED_CAST")
    val parentActivity: A
        get() {
            return activity as A
        }
}